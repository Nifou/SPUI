#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright Nicolas Fouquet 2018

nfouquet@mailfence.com

This file is part of SPUI.

SPUI is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPUI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with SPUI.  If not, see http://www.gnu.org/licenses/agpl-3.0.html.
"""
import wx
import os
import scandir
import httplib
import shutil
import random
import time
from SPUIS.SPUIS import spuis

class MainWindow(wx.Frame):
    y = 40
    mainSizer = None
    sw = None
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(1000,700))

        self.dirname=''
        self.firstOpen = True
        self.numberFunc = 0

        self.SetIcon(wx.Icon("../SPUI.png"))

        MainWindow.mainSizer = wx.BoxSizer(wx.VERTICAL)
        MainWindow.sw = wx.ScrolledWindow(self)

        self.url = wx.TextCtrl(self.sw, size=(1000, 30), pos=(0, 0), style=wx.TE_PROCESS_ENTER)

        # Events.
        self.Bind(wx.EVT_TEXT_ENTER, self.OnUrlEnter, self.url)
        self.Bind(wx.EVT_CLOSE, self.remove)
        self.Show(True)

    def OnUrlEnter(self, evt):
        self.adr = self.url.GetValue()
        self.ScreenIMGLinks("main.spui")

    def Scrollbars(self):
        self.sw.SetScrollbars(50, 50, 50, 50)

    def Parse(self, txt):
        lines = txt.split("\n")
        if self.firstOpen == False:
            for child in self.mainSizer.GetChildren():
                self.mainSizer.Layout()
                self.numberFunc = 0
                MainWindow.y = 40
        self.SetSizer(self.mainSizer)
        for li in lines:
            line = li.split(" ")
            name = line[0]

            if self.firstOpen == False:
                print("On LOOP : " + li)

            #Test names
            if name == "TEXT":
                print("TEXT")
                del line[0]
                lineJoin = " ".join(line)
                text = wx.StaticText(self.sw,label=lineJoin, pos=(0, self.y))
                self.mainSizer.Add(text, 0, wx.ALL, 5)
                MainWindow.y += 20

            if name == "IMG":
                print("IMG")
                line[1] = "files/imgs/" + line[1].rstrip('\n')

                png = wx.Image(line[1], wx.BITMAP_TYPE_ANY).ConvertToBitmap()
                img = wx.StaticBitmap(self.sw, -1, png, (0, self.y), (png.GetWidth(), png.GetHeight()))
                self.mainSizer.Add(img, 0, wx.ALL, 5)
                MainWindow.y += png.GetHeight()

            if name == "BUTTON":
                print("BUTTON")
                line[1] = line[1].rstrip('\n')

                self.textButton = line[1]
                self.button = wx.Button(self.sw, 6, self.textButton, (0,self.y), (70, 20))

                self.mainSizer.Add(self.button, 0, wx.ALL, 5)
                MainWindow.y += 30

                bind = "self.button.Bind(wx.EVT_BUTTON, OnButtonClick" + str(self.numberFunc) + ")"
                exec(bind)
                self.numberFunc += 1

            if name == "LINK":
                del line[0]
                line[0] = line[0].rstrip('\n')

                text = line[0]
                src = line[1]
                randomId = random.randint(1, 999999999)

                textLink = wx.StaticText(self.sw, -1, text, (0, self.y))

                font = wx.Font(14, wx.DECORATIVE, wx.ITALIC, wx.NORMAL, True)
                textLink.SetForegroundColour((51,153,255))
                textLink.SetFont(font)

                self.mainSizer.Add(textLink, 0, wx.ALL, 5)

                exec("def changePage" + str(randomId) + "(evt):\n\tframe.ScreenIMGLinks('" + src + "')")
                exec("textLink.Bind(wx.EVT_LEFT_DOWN, changePage" + str(randomId) + ")")
                MainWindow.y += 50

            if name == "EXTERN":
                if line[1] == "CODE":
                    line[2] = line[2].rstrip('\n')

                    result = spuis("files/spuis/" + line[2])
                    exec(result)

                if line[1] == "STYLE":
                    line[1] = line[2].rstrip('\n')
                    codeFile = open(line[2], "r")
        if self.firstOpen == True:
            print("SCROLLBARS")
            self.Scrollbars()
        print("END")
        self.firstOpen = False
        print("---END 2---")

    def ScreenIMGLinks(self, fileName):
        MainWindow.y = 0
        conn = httplib.HTTPConnection(self.adr + ":10000")

        #------------------IMG------------------
        conn.request("POST", "/", "LISTIMG")
        resIMG = conn.getresponse().read()
        exec("response = " + resIMG)

        if not os.path.exists('files'):
            os.makedirs('files')

        if not os.path.exists('files/imgs'):
            os.makedirs('files/imgs')

        for nameIMG in response:
            conn.request("GET", nameIMG)
            contentImg = conn.getresponse().read()

            file = open(nameIMG, "wb")
            file.write(contentImg)
            file.close()

        #------------------LINKS------------------
        conn.request("POST", "/", "LISTLINKS")
        resSPUIS = conn.getresponse().read()
        exec("responseSPUIS = " + resSPUIS)

        if not os.path.exists('files/spuis'):
            os.makedirs('files/spuis')

        for nameSPUIS in responseSPUIS:
            conn.request("GET", nameSPUIS)
            contentSPUIS = conn.getresponse().read()

            file = open(nameSPUIS, "wb")
            file.write(contentSPUIS)
            file.close()

        #------------------SCREEN------------------
        conn.request("POST", "/", "SCREEN")
        resSPUI = conn.getresponse().read()
        exec("responseSPUI = " + resSPUI)

        if not os.path.exists('files/spui'):
            os.makedirs('files/spui')

        for nameSPUI in responseSPUI:
            conn.request("GET", nameSPUI)
            contentSPUI = conn.getresponse().read()

            file = open(nameSPUI, "wb")
            file.write(contentSPUI)
            file.close()

        conn.close()

        screen = open("files/spui/" + fileName, "rb")
        self.Parse(screen.read())
        screen.close()


    def remove(self, evt):
        self.removeAllFiles()
        self.Destroy()

    def removeAllFiles(self):
        shutil.rmtree("files")

app = wx.App(False)
frame = MainWindow(None, "SPUI")
app.MainLoop()
