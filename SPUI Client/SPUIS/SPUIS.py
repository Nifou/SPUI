def spuis(path):
    result = ""

    fn = False

    codeFile = open(path, "r")
    lines = codeFile.readlines()
    codeFile.close()

    for li in lines:
        line = li.split(" ")
        line[0] = line[0].rstrip('\n')
        com = line[0]

        #Words occupeds
        if com == "fn":
            del line[0]
            lineJoin = " ".join(line)
            result += "def " + lineJoin + "\n"
            fn = True
        if com == "end":
            fn = False

        #Commands in functions
        if fn == True:
            if com == "say":
                del line[0]
                lineJoin = " ".join(line)

                result += "\tprint(" + lineJoin + ")\n"
            if com == "display":
                line[1] = line[1].rstrip('\n')
                line[2] = line[2].rstrip('\n')

                if line[1] == "text":
                    del line[0]
                    del line[0]
                    lineJoin = " ".join(line)
                    lineJoin = lineJoin.rstrip('\n')

                    result += "\ttext = wx.StaticText(MainWindow.sw, label='" + lineJoin + "', pos=(0, MainWindow.y))\n\tMainWindow.mainSizer.Add(text, 0, wx.ALL, 5)\n"
                elif line[1] == "img":
                    del line[0]
                    del line[0]
                    lineJoin = " ".join(line)
                    lineJoin = lineJoin.rstrip('\n')
                    result += "\tpng = wx.Image('" + lineJoin + "', wx.BITMAP_TYPE_ANY).ConvertToBitmap()\n\timg = wx.StaticBitmap(MainWindow.sw, -1, png, (0, MainWindow.y), (png.GetWidth(), png.GetHeight()))\n\tMainWindow.mainSizer.Add(img, 0, wx.ALL, 5)\n\tMainWindow.y += png.GetHeight()\n"
            if com == "dialog":
                line[1] = line[1].rstrip('\n')
                if line[1] == "filemanager":
                    result += "\tdlg = wx.FileDialog(MainWindow.sw, 'Choose a file', '', '', '*.*', wx.FD_OPEN)\n\tif dlg.ShowModal() == wx.ID_OK:\n\t\tOnFileManager(dlg.GetFilename(), dlg.GetDirectory())\n\tdlg.Destroy()\n"
        #Commands outside functions
        if fn == False:
            if com == "say":
                del line[0]
                lineJoin = " ".join(line)

                result += "print(" + lineJoin + ")\n"
            if com == "display":
                line[1] = line[1].rstrip('\n')
                line[2] = line[2].rstrip('\n')

                if line[1] == "text":
                    del line[0]
                    del line[0]
                    lineJoin = " ".join(line)
                    lineJoin = lineJoin.rstrip('\n')

                    result += "text = wx.StaticText(MainWindow.sw, label='" + lineJoin + "', pos=(0, MainWindow.y))\nMainWindow.mainSizer.Add(text, 0, wx.ALL, 5)\n"
                elif line[1] == "img":
                    del line[0]
                    del line[0]
                    lineJoin = " ".join(line)
                    lineJoin = lineJoin.rstrip('\n')
                    result += "png = wx.Image('" + lineJoin + "', wx.BITMAP_TYPE_ANY).ConvertToBitmap()\nimg = wx.StaticBitmap(MainWindow.sw, -1, png, (0, MainWindow.y), (png.GetWidth(), png.GetHeight()))\nMainWindow.mainSizer.Add(img, 0, wx.ALL, 5)\nMainWindow.y += png.GetHeight()\n"

    return result
