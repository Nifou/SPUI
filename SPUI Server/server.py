#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright Nicolas Fouquet 2018

nfouquet@mailfence.com

This file is part of SPUI.

SPUI is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SPUI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with SPUI.  If not, see http://www.gnu.org/licenses/agpl-3.0.html.
"""

import scandir
from http.server import BaseHTTPRequestHandler, HTTPServer

arrayIMG = []
arraySPUIS = []
arraySPUI = []
PORT = 10000

imgs = scandir.scandir("files/imgs")
spuis = scandir.scandir("files/spuis")
spui = scandir.scandir("files/spui")

for img in imgs:
    if not img.name.startswith("."):
        arrayIMG.append("files/imgs/" + img.name)

for file in spuis:
    if not file.name.startswith("."):
        arraySPUIS.append("files/spuis/" + file.name)

for fileS in spui:
    if not fileS.name.startswith("."):
        arraySPUI.append("files/spui/" + fileS.name)

class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.wfile.write(open(self.path, "rb").read())
    def do_POST(self):
        length = int(self.headers['Content-length'])
        task = self.rfile.read(length)
        self.send_response(200, "OK")
        self.end_headers()

        if task == "LISTIMG":
            self.wfile.write(arrayIMG)
        if task == "LISTLINKS":
            self.wfile.write(arraySPUIS)
        if task == "SCREEN":
            self.wfile.write(arraySPUI)

try:
	server = HTTPServer(('', PORT), myHandler)
	print('Server start successfully on port ' + str(PORT))
	server.serve_forever()

except KeyboardInterrupt:
	server.socket.close()

#Read file
#fileSPUI = open("main.spui", "r")
#contentSPUI = fileSPUI.read()
#fileSPUI.close()
